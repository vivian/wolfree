/* SPDX-License-Identifier: AGPL-3.0-or-later */

// @ts-check

const wolfreeLexicalScopeName = (() => {
  /** @type {string[]} */
  const possibleLetters = [];

  for (let i = 32; i < 127; i++) {
    possibleLetters.push(String.fromCharCode(i));
  }

  const possibleNames = possibleLetters.flatMap((x) =>
    possibleLetters.map((y) => x + y)
  );

  const isValid = (name = "") => {
    try {
      return (
        typeof eval(name).value === "string" &&
        typeof eval(name).i2d === "boolean"
      );
    } catch (error) {
      return false;
    }
  };

  const wolfreeLexicalScopeName = possibleNames.find(isValid);

  if (typeof wolfreeLexicalScopeName !== "string") {
    console.warn({ wolfreeLexicalScopeName });
    return "";
  }

  return wolfreeLexicalScopeName;
})();

(() => {
  const get = (params = "") => {
    return new URLSearchParams(location.search).get(params);
  };

  eval(wolfreeLexicalScopeName).value = get("i") || "topic mathematics";
  eval(wolfreeLexicalScopeName).i2d = get("i2d") === "true";
})();

/**
 * Regex in regex - Rust
 * https://docs.rs/regex/latest/regex/struct.Regex.html#replacement-string-syntax
 */
// @ts-ignore
$0;

(async () => {
  (
    await import(
      // @ts-ignore
      "/ajax/libs/wolfree/2023.8.31/js/entrypoint.js"
    )
  ).default({
    input: eval(wolfreeLexicalScopeName).value,
    ...(eval(wolfreeLexicalScopeName).i2d ? { i2d: true } : {}),
    podstate: ["Step-by-step solution", "Step-by-step", "Show all steps"],
  });
})();
