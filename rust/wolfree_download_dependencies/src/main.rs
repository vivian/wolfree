/* SPDX-License-Identifier: AGPL-3.0-or-later */

//! rust - Can Cargo download and build dependencies without also building the application? - Stack Overflow
//! <https://stackoverflow.com/questions/42130132/can-cargo-download-and-build-dependencies-without-also-building-the-application>
//!
//! cargo build --dependencies-only · Issue #2644 · rust-lang/cargo · GitHub
//! <https://github.com/rust-lang/cargo/issues/2644>

#![allow(clippy::blanket_clippy_restriction_lints)]
#![allow(clippy::implicit_return)]
#![allow(clippy::question_mark_used)]

use regex::Regex;
use std::error;
use walkdir::WalkDir;

/// Entry point of the program.
fn main() -> Result<(), Box<dyn error::Error>> {
    let _: Regex = Regex::new("")?;
    let _: WalkDir = WalkDir::new("");
    Ok(())
}
