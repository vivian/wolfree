/* SPDX-License-Identifier: AGPL-3.0-or-later */

//! Customize Wolfree instances to suit Libredirect.

#![allow(clippy::blanket_clippy_restriction_lints)]
#![allow(clippy::implicit_return)]
#![allow(clippy::question_mark_used)]

use std::error;
use std::fs;

/// By default, the main function do nothing as most Wolfree users do not use Libredirect.
#[allow(unreachable_code)]
fn main() -> Result<(), Box<dyn error::Error>> {
    // Remove the following line to make the instances Libredirect-compatible.
    return Ok(());
    // Remove the previous line to make the instances Libredirect-compatible.

    // Disable the entrypoint of the JavaScript modules.
    fs::write(
        "./docusaurus/static/ajax/libs/wolfree/2023.8.31/js/entrypoint.js",
        "export default () => {};"
    )?;

    fs::write(
        "./docusaurus/wolfree.config.js",
        "module.exports = { config: { libredirect: true } };"
    )?;

    fs::write(
        "./docusaurus/static/instances.json",
        include_str!("./docusaurus/static/instances.json")
    )?;

    fs::write("./docusaurus/docs/index.mdx", include_str!("./docusaurus/docs/index.mdx"))?;
    fs::write("./docusaurus/docs/mirror.mdx", include_str!("./docusaurus/docs/mirror.mdx"))?;
    fs::write("./docusaurus/docs/source.mdx", include_str!("./docusaurus/docs/source.mdx"))?;

    Ok(())
}
