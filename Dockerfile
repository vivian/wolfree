# SPDX-License-Identifier: AGPL-3.0-or-later

FROM alpine AS stage-alpine

# alpine - Official Image | Docker Hub
# https://hub.docker.com/_/alpine

WORKDIR /app/

# Install all dependencies, including Wget, Rust, Docusaurus, and other required packages.

RUN \
apk \
-- \
add \
wget \
gcc \
rustup \
npm \
;

# Install the dependencies of Docusaurus and Typescript.

COPY \
./docusaurus/package.json \
./docusaurus/package.json

RUN \
npm \
--prefix ./docusaurus/ \
-- \
install \
;

RUN \
npm \
--prefix ./docusaurus/ \
-- \
audit \
fix \
;

COPY \
./typescript/package.json \
./typescript/package.json

RUN \
npm \
--prefix ./typescript/ \
-- \
install \
;

RUN \
npm \
--prefix ./typescript/ \
-- \
audit \
fix \
;

# Set up the Rust compiler.

RUN \
rustup-init \
-y \
-- \
;

# Download the necessary dependencies for the Rust programs.

COPY \
./rust/wolfree_download_dependencies/ \
./rust/wolfree_download_dependencies/

RUN \
PATH="$HOME/.cargo/bin:$PATH" \
cargo \
-- \
clippy \
--manifest-path ./rust/wolfree_download_dependencies/Cargo.toml \ 
-- \
--warn clippy::all \
--warn clippy::cargo \
--warn clippy::nursery \
--warn clippy::pedantic \
--warn clippy::restriction \
;

RUN \
PATH="$HOME/.cargo/bin:$PATH" \
cargo \
run \
--manifest-path ./rust/wolfree_download_dependencies/Cargo.toml \
-- \
;

# Cloudflare and WolframAlpha are invaluable tools for web developers,
# providing easy access to prebuilt JavaScript files.
# To effortlessly obtain these files, you can utilize the Wget command-line utility.
# Cloudflare's web servers offer a vast collection of popular JavaScript libraries.
# On the other hand,
# WolframAlpha's web servers specifically host the essential JavaScript files for the 2-dimensional math input editor.

RUN \
wget \
--directory-prefix=./docusaurus/static/ \
--no-host-directories \
--page-requisites \
--convert-links \
--no-verbose \
--input-file=- \
--base= \
-- \
'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js' \
'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.map' \
'https://cdnjs.cloudflare.com/ajax/libs/dompurify/3.0.5/purify.min.js' \
'https://cdnjs.cloudflare.com/ajax/libs/dompurify/3.0.5/purify.min.js.map' \
'https://www.wolframalpha.com/input/index.html' \
; \
: \
;

# We use the Sed command to extract the pathnames from the "Webpack" and "buildManifest" files.
# After successfully parsing the pathnames, we proceed to download the files using the Wget command.

RUN \
sed \
-e 's|.*js...static.chunks...........||' \
-e 's|"........js.....miniCssF.*|.js\n|' \
-e 's|:"|.|g' \
-e 's|",|.js\n|g' \
-- \
./docusaurus/static/_next/static/chunks/webpack-*.js \
| \
wget \
--directory-prefix=./docusaurus/static/ \
--no-host-directories \
--page-requisites \
--convert-links \
--no-verbose \
--input-file=- \
--base=https://www.wolframalpha.com/_next/static/chunks/ \
-- \
; \
: \
;

RUN \
sed \
-e 's|.*return.static.css.....||' \
-e 's|.........css.......function.*|.css\n|' \
-e 's|[0-9]*:"||g' \
-e 's|",|.css\n|g' \
-- \
./docusaurus/static/_next/static/chunks/webpack-*.js \
| \
wget \
--directory-prefix=./docusaurus/static/ \
--no-host-directories \
--page-requisites \
--convert-links \
--no-verbose \
--input-file=- \
--base=https://www.wolframalpha.com/_next/static/css/ \
-- \
; \
: \
;

RUN \
sed \
-e 's|.*sortedPages.*}..||' \
-e 's|...self.__BUILD_MANIFEST_CB&&self.__BUILD_MANIFEST_CB.*||' \
-e 's|","|\n|g' \
-- \
./docusaurus/static/_next/static/*/_buildManifest.js \
| \
wget \
--directory-prefix=./docusaurus/static/ \
--no-host-directories \
--page-requisites \
--convert-links \
--no-verbose \
--input-file=- \
--base=https://www.wolframalpha.com/_next/ \
-- \
; \
: \
;

# Remove unused large files,
# such as decorative images,
# from the static website.

RUN \
truncate \
-s 0 \
-- \
./docusaurus/static/_next/static/images/* \
./docusaurus/static/_next/static/media/*.png \
./docusaurus/static/_next/static/media/*.gif \
;

# Compile and execute the Rust program.
# The Rust program allows customization of the static pages downloaded with Wget.

COPY \
./rust/wolfree_sed_in_place/ \
./rust/wolfree_sed_in_place/

RUN \
PATH="$HOME/.cargo/bin:$PATH" \
cargo \
-- \
clippy \
--manifest-path ./rust/wolfree_sed_in_place/Cargo.toml \
-- \
--warn clippy::all \
--warn clippy::cargo \
--warn clippy::nursery \
--warn clippy::pedantic \
--warn clippy::restriction \
;

RUN \
PATH="$HOME/.cargo/bin:$PATH" \
cargo run \
--manifest-path ./rust/wolfree_sed_in_place/Cargo.toml \
-- \
;

# Compile the Typescript files and output the JavaScript files.

COPY \
./typescript/ \
./typescript/

RUN \
npm \
--prefix ./typescript/ \
-- \
exec \
tsc \
--project ./typescript/ \
;

# Preprocess MDX files.

COPY \
./docusaurus/ \
./docusaurus/

# Run the Rust program to customize the Docusaurus pages.

COPY \
./rust/wolfree_patch_libredirect/ \
./rust/wolfree_patch_libredirect/

RUN \
PATH="$HOME/.cargo/bin:$PATH" \
cargo \
-- \
clippy \
--manifest-path ./rust/wolfree_patch_libredirect/Cargo.toml \
-- \
--warn clippy::all \
--warn clippy::cargo \
--warn clippy::nursery \
--warn clippy::pedantic \
--warn clippy::restriction \
;

RUN \
PATH="$HOME/.cargo/bin:$PATH" \
cargo run \
--manifest-path ./rust/wolfree_patch_libredirect/Cargo.toml \
-- \
;

# Create a webpage that lists all repositories and instructions for self-hosting web services.

COPY \
./README.md \
./README.md

RUN \
cat \
-- \
./README.md \
>> \
./docusaurus/docs/source.mdx \
;

# Use Docusaurus to build the static website.

RUN \
npm \
--prefix ./docusaurus/ \
-- \
run \
build \
;

# Host the static content with a webserver.

CMD \
npm \
--prefix ./docusaurus/ \
-- \
run \
serve \
--host localhost \
--port 80 \
;

EXPOSE 80

# TODO: multi-stage
# https://docs.docker.com/build/building/multi-stage/
# https://docs.docker.com/build/guide/multi-stage/

# Optimizing builds with cache management | Docker Documentation
# https://docs.docker.com/build/cache/
# Because a change causes a rebuild for steps that follow,
# try to make expensive steps appear near the beginning of the Dockerfile.
# Steps that change often should appear near the end of the Dockerfile to avoid triggering rebuilds of layers that haven't changed.
