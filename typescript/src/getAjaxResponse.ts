/* SPDX-License-Identifier: AGPL-3.0-or-later */

import AjaxResponse from "./AjaxResponse.js";
import type AjaxSettings from "./AjaxSettings.js";

export default async (AjaxSettings: AjaxSettings): Promise<AjaxResponse> => {
  try {
    Object.freeze(AjaxSettings);

    // npm i @types/jquery
    return Object.freeze(await jQuery.ajax(AjaxSettings));
  } catch (error) {
    console.warn({ error });
    return {};
  }
};
