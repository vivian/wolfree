/* SPDX-License-Identifier: AGPL-3.0-or-later */

import AppID from "./AppID.js";
import typescriptNever from "./typescriptNever.js";

/**
 * To generate a new AppID, please follow these steps:
 *
 *  1. Open Tor Browser and go to:
 *     https://products.wolframalpha.com/api/
 *  2. Click the orange "Get API Access" button.
 *     Tor Browser will redirect you to:
 *     https://account.wolfram.com/login/oauth2/sign-in
 *  3. Click the red "Create one" hyperlink to create a new Wolfram ID.
 *     Tor Browser will redirect you to:
 *     https://account.wolfram.com/login/create
 *  4. Fill out the form with random alphanumeric characters.
 *  5. Click the red "Create Wolfram ID" button.
 *     Tor Browser will redirect you to:
 *     https://developer.wolframalpha.com/portal/myapps/index.html
 *  6. Click the orange "Sign up to get your first AppID" button.
 *  7. Fill out the form with random alphanumeric characters.
 *  8. Click the orange "Sign up" button.
 *  9. Click the orange "Get an AppID" button.
 * 10. Fill out the form with random alphanumeric characters.
 * 11. Click the orange "Get AppID" button.
 */
const appIDArray: Readonly<AppID[]> = Object.freeze([
  "G5VJT6-29G98TJJAG",
]);

console.assert(appIDArray.length > 0);

appIDArray.forEach((appID) => {
  console.assert(appID.length === 17);
  console.assert(/[0-9A-Z]{6}-[0-9A-Z]{10}/.test(appID));
});

export default (): AppID => {
  const random = appIDArray[getRandomInt() % appIDArray.length];

  if (typeof random === "string") {
    return random;
  } else if (random === undefined) {
    console.warn({ random });
  } else {
    typescriptNever(random);
  }

  return "G5VJT6-29G98TJJAG";
};

const getRandomInt = (): number => {
  const random = crypto.getRandomValues(new Uint32Array(1))[0];

  if (typeof random === "number") {
    return random;
  } else if (random === undefined) {
    console.warn({ random });
  } else {
    typescriptNever(random);
  }

  return 0;
};
