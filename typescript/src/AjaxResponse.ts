/* SPDX-License-Identifier: AGPL-3.0-or-later */

import type Pod from "./Pod.js";

export default AjaxResponse;

type AjaxResponse = Readonly<{
  queryresult?: {
    pods?: Pod[];
  };
}>;
