/* SPDX-License-Identifier: AGPL-3.0-or-later */

export default State;

type State = Readonly<{
  value?: string;
  states?: {
    name?: string;
  }[];
}>;
