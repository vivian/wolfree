/* SPDX-License-Identifier: AGPL-3.0-or-later */

export default `
  <div class="wolfree-pods wolfree-placeholder">
    <div>
      <div>
        <div><div></div></div>
        <div><div></div></div>
        <div><div></div></div>
      </div>
    </div>
  </div>
`;
