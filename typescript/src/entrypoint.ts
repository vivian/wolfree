/* SPDX-License-Identifier: AGPL-3.0-or-later */

import type EntrypointParameter from "./EntrypointParameter.js";
import getAjaxResponse from "./getAjaxResponse.js";
import getAjaxSettings from "./getAjaxSettings.js";
import getNewHTML from "./getNewHTML.js";
import listenToChangeEvent from "./listenToChangeEvent.js";
import placeholder from "./placeholder.js";
import reRenderComponent from "./reRenderComponent.js";
import setContentEditable from "./setContentEditable.js";
import typescriptExhaustive from "./typescriptExhaustive.js";

export default async (
  EntrypointParameter: EntrypointParameter
): Promise<void> => {
  try {
    Object.freeze(EntrypointParameter);
    typescriptExhaustive(EntrypointParameter);

    window.scroll(0, 0);
    reRenderComponent(placeholder);

    const AjaxSettings = getAjaxSettings(EntrypointParameter);
    const AjaxResponse = await getAjaxResponse(AjaxSettings);
    const newHTML = getNewHTML(AjaxSettings, AjaxResponse);
    reRenderComponent(newHTML);

    listenToChangeEvent(EntrypointParameter);
    setContentEditable();
  } catch (error) {
    console.warn({ error });
  }
};
