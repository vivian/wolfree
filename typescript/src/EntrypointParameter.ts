/* SPDX-License-Identifier: AGPL-3.0-or-later */

export default EntrypointParameter;

type EntrypointParameter = Readonly<{
  input: string;
  i2d?: true;
  podstate: Readonly<
    ["Step-by-step solution", "Step-by-step", "Show all steps", string?]
  >;
}>;
