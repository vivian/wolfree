/* SPDX-License-Identifier: AGPL-3.0-or-later */

export default SubPod;

type SubPod = Readonly<{
  img?: {
    src?: string;
    alt?: string;
  };
  plaintext?: string;
}>;
