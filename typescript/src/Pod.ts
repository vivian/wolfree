/* SPDX-License-Identifier: AGPL-3.0-or-later */

import type State from "./State.js";
import type SubPod from "./SubPod.js";

export default Pod;

type Pod = Readonly<{
  title?: string;
  states?: State[];
  subpods?: SubPod[];
}>;
