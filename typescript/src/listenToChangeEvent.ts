/* SPDX-License-Identifier: AGPL-3.0-or-later */

import type EntrypointParameter from "./EntrypointParameter.js";
import entrypoint from "./entrypoint.js";
import podsClassName from "./podsClassName.js";
import typescriptNever from "./typescriptNever.js";

export default (EntrypointParameter: EntrypointParameter): void => {
  try {
    Object.freeze(EntrypointParameter);

    Array.from(
      document.querySelectorAll(
        `html > body > div#__next > div > main > main > div.${podsClassName} > div > div > section > section > div:is(:first-child) > select`
      )
    )
      .filter((element: Element): boolean => {
        if (element instanceof HTMLSelectElement) {
          return true;
        } else if (element instanceof Element) {
          console.warn({ element });
        } else {
          typescriptNever(element);
        }

        return false;
      })
      .map((element: Element): void => {
        element.addEventListener("change", async (event): Promise<void> => {
          if (event.target instanceof HTMLSelectElement) {
            await entrypoint({
              ...EntrypointParameter,
              podstate: [
                "Step-by-step solution",
                "Step-by-step",
                "Show all steps",
                event.target.value,
              ],
            });
          } else if (event.target instanceof EventTarget) {
            console.warn({ event });
          } else if (event.target === null) {
            console.warn({ event });
          } else {
            typescriptNever(event.target);
          }
        });
      });
  } catch (error) {
    console.warn({ error });
  }
};
