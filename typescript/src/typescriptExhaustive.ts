/* SPDX-License-Identifier: AGPL-3.0-or-later */

import type EntrypointParameter from "./EntrypointParameter.js";
import typescriptNever from "./typescriptNever.js";

export default (EntrypointParameter: EntrypointParameter): void => {
  Object.freeze(EntrypointParameter);

  if (typeof EntrypointParameter.input !== "string") {
    typescriptNever(EntrypointParameter.input);
  }

  if (EntrypointParameter.i2d !== true) {
    if (EntrypointParameter.i2d !== undefined) {
      typescriptNever(EntrypointParameter.i2d);
    }
  }

  if (!(EntrypointParameter.podstate instanceof Array)) {
    typescriptNever(EntrypointParameter.podstate);
  }

  if (EntrypointParameter.podstate.length !== 3) {
    if (EntrypointParameter.podstate.length !== 4) {
      typescriptNever(EntrypointParameter.podstate.length);
    }
  }

  if (EntrypointParameter.podstate[0] !== "Step-by-step solution") {
    typescriptNever(EntrypointParameter.podstate[0]);
  }

  if (EntrypointParameter.podstate[1] !== "Step-by-step") {
    typescriptNever(EntrypointParameter.podstate[1]);
  }

  if (EntrypointParameter.podstate[2] !== "Show all steps") {
    typescriptNever(EntrypointParameter.podstate[2]);
  }

  if (typeof EntrypointParameter.podstate[3] !== "string") {
    if (typeof EntrypointParameter.podstate[3] !== "undefined") {
      typescriptNever(EntrypointParameter.podstate[3]);
    }
  }
};
