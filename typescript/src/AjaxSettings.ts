/* SPDX-License-Identifier: AGPL-3.0-or-later */

import AppID from "./AppID.js";
import EntrypointParameter from "./EntrypointParameter.js";

export default AjaxSettings;

type AjaxSettings = Readonly<{
  url: "https://api.wolframalpha.com/v2/query";
  dataType: "jsonp";
  traditional: true;
  data: Readonly<
    EntrypointParameter & {
      appid: AppID;
      output: "json";
      reinterpret: true;
      podtimeout: 30;
      scantimeout: 30;
      parsetimeout: 30;
      totaltimeout: 30;
      formattimeout: 30;
    }
  >;
}>;

// Wolfram|Alpha Full Results API Reference
// https://products.wolframalpha.com/api/documentation

// jQuery.ajax() | jQuery API Documentation
// https://api.jquery.com/jQuery.ajax/
