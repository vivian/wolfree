/* SPDX-License-Identifier: AGPL-3.0-or-later */

import podsClassName from "./podsClassName.js";
import typescriptNever from "./typescriptNever.js";

export default (): void => {
  try {
    Array.from(
      document.querySelectorAll(
        `html > body > div#__next > div > main > main > div.${podsClassName} > div > div > section > section > div > div > div > details > div`
      )
    )
      .filter((element: Element): boolean => {
        if (element instanceof HTMLDivElement) {
          return true;
        } else if (element instanceof Element) {
          console.warn({ element });
        } else {
          typescriptNever(element);
        }

        return false;
      })
      .map((element: Element): void => {
        if (element instanceof Element) {
          element.setAttribute("contenteditable", "");
        } else {
          typescriptNever(element);
        }
      });
  } catch (error) {
    console.warn({ error });
  }
};
