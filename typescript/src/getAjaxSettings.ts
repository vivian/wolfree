/* SPDX-License-Identifier: AGPL-3.0-or-later */

import AjaxSettings from "./AjaxSettings.js";
import EntrypointParameter from "./EntrypointParameter.js";
import getAppID from "./getAppID.js";

export default (EntrypointParameter: EntrypointParameter): AjaxSettings =>
  Object.freeze({
    url: "https://api.wolframalpha.com/v2/query",
    dataType: "jsonp",
    traditional: true,
    data: Object.freeze({
      ...EntrypointParameter,
      appid: getAppID(),
      output: "json",
      reinterpret: true,
      podtimeout: 30,
      scantimeout: 30,
      parsetimeout: 30,
      totaltimeout: 30,
      formattimeout: 30,
    }),
  });
