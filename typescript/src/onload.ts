/* SPDX-License-Identifier: AGPL-3.0-or-later */

import typescriptNever from "./typescriptNever.js";

export default addEventListener(
  "load",
  (): void => (
    setTimeout(
      (): void =>
        console.assert(
          Array.from(
            document.querySelectorAll(
              // To select the form element, we use different selectors depending on the input mode.
              // If the input mode is "natural language", the form is a direct child of the section element, so we use the selector "section > form".
              // However, if the input mode is "math input", the form is a direct child of the div element, so we use the selector "div > form".
              `
                html > body > #__next > div > main > main > div > div > section > form > div > div > input,
                html > body > #__next > div > main > main > div > div > div       form > div > div > input
              `
            )
          )
            .filter((element: Element): boolean => {
              if (element instanceof HTMLInputElement) {
                return true;
              } else if (element instanceof Element) {
                console.warn({ element });
              } else {
                typescriptNever(element);
              }

              return false;
            })
            .map((element: Element): void => {
              if (element instanceof HTMLElement) {
                element.focus();
              } else if (element instanceof Element) {
                console.warn({ element });
              } else {
                typescriptNever(element);
              }
            }).length === 1
        ),
      1000
    ),
    [
      (): void =>
        Array.from(
          document.querySelectorAll(
            // The positioning of the ul element is dynamically adjusted to ensure it adapts well to different viewport widths.
            // To specifically target the ul element when the viewport width is larger, we use the selector "div:is(:first-child) > ul".
            // Conversely, to target the ul element when the viewport width is smaller, we use the selector "div:is(:first-child) + ul".
            `
              html > body > #__next > div > main > main > div > div > div > section > section > div:is(:first-child) > ul > li,
              html > body > #__next > div > main > main > div > div > div > section > section > div:is(:first-child) + ul > li
            `
          )
        )
          .filter((element: Element): boolean => {
            if (element instanceof HTMLLIElement) {
              return true;
            } else if (element instanceof Element) {
              console.warn({ element });
            } else {
              typescriptNever(element);
            }

            return false;
          })
          .forEach((element: Element): void => {
            if (element instanceof HTMLElement) {
              if (element.innerHTML.includes("Step-by-step")) {
                element.style.display = "none";
              } else {
              }
            } else if (element instanceof Element) {
              console.warn({ element });
            } else {
              typescriptNever(element);
            }
          }),
      (): void => {
        document.title = document.title.replace(
          "- Wolfram|Alpha",
          "- Free Wolfram|Alpha Step-by-step Solution - Wolfree"
        );
      },
    ].map(
      (callback: () => void): void => (
        setInterval(callback, 2000), addEventListener("click", callback)
      )
    ),
    scroll(0, 0)
  )
);
