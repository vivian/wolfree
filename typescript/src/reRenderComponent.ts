/* SPDX-License-Identifier: AGPL-3.0-or-later */

import podsClassName from "./podsClassName.js";
import typescriptNever from "./typescriptNever.js";

export default (newHTML: string): void => {
  try {
    console.assert(
      Array.from(
        document.querySelectorAll(
          `html > body > #__next > div > main > main > div.${podsClassName}`
        )
      )
        .filter((element: Element): boolean => {
          if (element instanceof HTMLDivElement) {
            return true;
          } else if (element instanceof Element) {
            console.warn({ element });
          } else {
            typescriptNever(element);
          }

          return false;
        })
        .map((element: Element): void => {
          if (element instanceof Element) {
            element.remove();
          } else {
            typescriptNever(element);
          }
        }).length <= 1
    );

    console.assert(
      Array.from(
        document.querySelectorAll(
          "html > body > #__next > div > main > main > div:nth-of-type(1)"
        )
      )
        .filter((element: Element): boolean => {
          if (element instanceof HTMLDivElement) {
            return true;
          } else if (element instanceof Element) {
            console.warn({ element });
          } else if (element === null) {
            console.warn({ element });
          } else {
            typescriptNever(element);
          }

          return false;
        })
        .map((element: Element): void => {
          if (element instanceof Element) {
            element.insertAdjacentHTML(
              "afterend",
              // npm i @types/dompurify
              globalThis.DOMPurify.sanitize(newHTML)
            );
          } else if (element === null) {
            console.warn({ element });
          } else {
            typescriptNever(element);
          }
        }).length === 1
    );
  } catch (error) {
    console.warn({ error });
  }
};
