/* SPDX-License-Identifier: AGPL-3.0-or-later */

// @ts-check

/** @type { { config: { libredirect: false } } } */
const wolfree = require("./wolfree.config.js");
const {themes} = require('prism-react-renderer');
const lightCodeTheme = themes.github;
const darkCodeTheme = themes.dracula;

/** @type {import('@docusaurus/types').Config} */
module.exports = {
  title: wolfree.config.libredirect
    ? "Paywall-respecting instances: Wolfree instances that do not bypass paywalls"
    : "Wolfree - Free WolframAlpha step-by-step solution",
  url: "http://example.com",
  baseUrl: "/",
  favicon: "data:,",
  presets: [
    [
      "classic",
      /** @type {import('@docusaurus/preset-classic').Options} */
      {
        docs: {
          sidebarPath: require.resolve("./sidebars.config.js"),
          routeBasePath: "/",
          breadcrumbs: false,
        },
        blog: false,
        pages: false,
        theme: {
          customCss: [require.resolve("./src/css/custom.css")],
        },
      },
    ],
  ],
  /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
  themeConfig: {
    colorMode: {
      disableSwitch: true,
    },
    docs: {
      sidebar: {
        hideable: true,
      },
    },
    navbar: {
      hideOnScroll: true,
      items: [
        {
          to: "/",
          label: wolfree.config.libredirect
            ? "Paywall-respecting instances"
            : "Wolfree",
          position: "left",
        },
        {
          to: "pathname:///input/",
          label: "Input",
          position: "right",
          target: "_self",
        },
      ],
    },
    footer: {
      links: [
        {
          label: "Home page",
          href: "/",
        },
        {
          html: '<a class="footer__link-item" href="/input/">Input math problems</a>',
        },
        {
          label: "Mirror site",
          href: "/mirror",
        },
        {
          label: "Source code",
          href: "/source",
        },
        {
          label: "Community",
          href: "/community",
        },
        {
          label: "Acknowledgment",
          href: "/acknowledgment",
        },
      ],
      copyright: "SPDX-License-Identifier: AGPL-3.0-or-later",
    },
    prism: {
      theme: lightCodeTheme,
      darkTheme: darkCodeTheme,
    },
  },
};
