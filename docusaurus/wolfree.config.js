/* SPDX-License-Identifier: AGPL-3.0-or-later */

// dependents:
// ../rust/wolfree_patch_libredirect/src/main.rs
/** @type { { config: { libredirect: false } } } */
module.exports = { config: { libredirect: false } };
