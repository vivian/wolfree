/* SPDX-License-Identifier: AGPL-3.0-or-later */

import React from "react";
import typescriptNever from "../../functions/typescriptNever";

export default (): React.JSX.Element => {
  React.useEffect((): void => {
    const pathname = window.location.pathname;

    const isExample = pathname.includes("/example");

    if (isExample === true) {
      const topic = "topic " + pathname.split("/").pop().replace(/-/g, " ");
      const url = new URL("/input/", window.location.href);
      url.searchParams.set("i", topic);
      window.location.replace(url);
    } else if (isExample === false) {
      window.location.replace("/");
    } else {
      window.location.replace("/");
      typescriptNever(isExample);
    }
  }, []); // Empty dependency array means the effect runs only once after initial render.

  return <></>;
};

/*
 * test case:
 *
 *   - Passing
 *     - elementary-math
 *       - Goal: https://www.wolframalpha.com/examples/mathematics/elementary-math
 *       - Doesn't work: http://localhost/input?i=elementary+math
 *       - Does work: http://localhost/input?i=topic+elementary+math
 *     - common-core-math-functions
 *       - Goal: https://wc.wolframalpha.com/examples/mathematics/common-core-math/common-core-math-functions
 *       - Doesn't work: http://localhost/input?i=common+core+math+functions
 *       - Does work: http://localhost/input?i=topic+common+core+math+functions
 *
 *   - Failing
 *     - continuity
 *       - Goal: https://www.wolframalpha.com/examples/mathematics/calculus-and-analysis/continuity
 *       - Does not work: http://localhost/input?i=topic+continuity
 *       - Does work: (not found yet)
 *     - neuroscience
 *       - Goal: https://www.wolframalpha.com/examples/science-and-technology/life-sciences/neuroscience
 *       - Does not work: http://localhost/input?i=topic+neuroscience
 *       - Does work: (not found yet)
 *     - molecular-biology
 *       - Goal: https://wc.wolframalpha.com/examples/science-and-technology/life-sciences/molecular-biology
 *       - Does not work: http://localhost/input?i=topic+molecular+biology
 *       - Does work: (not found yet)
 *     - personal-finance
 *       - Goal: https://www.wolframalpha.com/examples/everyday-life/personal-finance
 *       - Does not work: http://localhost/input?i=topic+personal+finance
 *       - Does work: (not found yet)
 *
 */
