/* SPDX-License-Identifier: AGPL-3.0-or-later */

import DDoSProtection from "./DDoSProtection";
import NotFound from "@theme-original/NotFound";
import React from "react";
import Redirection from "./Redirection";

export default (
  props: React.JSX.ElementAttributesProperty
): React.JSX.Element => (
  <>
    <Redirection />
    <DDoSProtection />
    <div style={{ display: "none" }}>
      <NotFound {...props} />
    </div>
  </>
);

// How can I customize the 404 page? · facebook/docusaurus · Discussion #6030
// https://github.com/facebook/docusaurus/discussions/6030
