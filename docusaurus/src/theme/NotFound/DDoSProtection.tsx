/* SPDX-License-Identifier: AGPL-3.0-or-later */

import React from "react";

export default (): React.JSX.Element => (
  <div
    style={{
      fontFamily:
        'system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,"Apple Color Emoji","Segoe UI Emoji",Segoe UI Symbol,"Noto Color Emoji"',
      //
      lineHeight: 1.15,
      // -webkit-text-size-adjust: 100%;
      // color: #313131;
      color: "black",
    }}
  >
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        minHeight: "100vh",
      }}
    >
      <div
        style={{
          display: "flex",
          flex: 1,
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <div
          style={{
            paddingRight: "1.5rem",
            paddingLeft: "1.5rem",
            //
            margin: "8rem auto",
            width: "100%",
            maxWidth: "60rem",
            //
            marginTop: " 4rem",
          }}
        >
          <div
            style={{
              marginBottom: "1rem",
              //
              overflowWrap: "break-word",
              //
              lineHeight: "1.75rem",
              fontSize: "1.5rem",
              //
              // line-height: 3.75rem;
              // font-size: 2.5rem;
              fontWeight: 500,
            }}
          >
            &nbsp;
          </div>
          <div
            style={{
              lineHeight: "1.5rem",
              fontSize: "1.25rem",
              //
              // line-height: 2.25rem;
              // font-size: 1.5rem;
              fontWeight: 500,
            }}
          >
            Checking if the site connection is secure
          </div>
          <div
            style={{
              display: "flex",
            }}
          >
            <div
              style={{
                flexWrap: "wrap",
                justifyContent: "center",
                //
                display: "flex",
                //
                margin: "2rem 0",
              }}
            >
              <div>
                <div
                  style={{
                    border: "none",
                    overflow: "hidden",
                    width: "300px",
                    height: "65px",
                  }}
                >
                  <div
                    style={{
                      margin: 0,
                      padding: 0,
                      width: "100%",
                      height: "100%",
                      overflow: "hidden",
                    }}
                  >
                    <div
                      style={{
                        backgroundColor: "#fff",
                        lineHeight: "17px",
                        // color: #1d1f20;
                        color: "black",
                        fontFamily:
                          "-apple-system,system-ui,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen,Ubuntu,Helvetica Neue,Arial,sans-serif",
                        fontSize: "14px",
                        fontWeight: 400,
                        // -webkit-font-smoothing: antialiased;
                        fontStyle: "normal",
                        //
                        margin: 0,
                        padding: 0,
                        width: "100%",
                        height: "100%",
                        overflow: "hidden",
                      }}
                    >
                      <div style={{ borderSpacing: 0 }}>
                        <div
                          style={{
                            display: "flex",
                            //
                            alignItems: "center",
                            justifyContent: "space-between",
                            border: "1px solid #e0e0e0",
                            backgroundColor: "#fafafa",
                            height: "63px",
                            userSelect: "none",
                          }}
                        >
                          <div
                            style={{
                              display: "flex",
                              alignItems: "center",
                              marginLeft: "11px",
                            }}
                          >
                            <div
                              style={{
                                display: "flex",
                                marginRight: "8px",
                                width: "30px",
                                height: "30px",
                                animation: "rotate 5s linear infinite",
                              }}
                            ></div>
                            <div>Verifying...</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div
            style={{
              overflowWrap: "break-word",
              //
              lineHeight: "1.5rem",
              fontSize: "1rem",
              //
              // line-height: 2.25rem;
              // font-size: 1.5rem;
              fontWeight: 400,
              //
              margin: "2rem 0",
            }}
          >
            Wolfree needs to review the security of your connection before
            proceeding.
          </div>
        </div>
      </div>
      <div
        style={{
          margin: "0 auto",
          width: "100%",
          maxWidth: "60rem",
          lineHeight: "1.125rem",
          fontSize: ".75rem",
          paddingRight: "1.5rem",
          paddingLeft: "1.5rem",
        }}
      >
        <div
          style={{
            borderTop: "1px solid #d9d9d9",
            paddingTop: "1rem",
            paddingBottom: "1rem",
          }}
        >
          <div style={{ textAlign: "center" }}>
            Performance & security by Wolfree
          </div>
        </div>
      </div>
    </div>
  </div>
);
