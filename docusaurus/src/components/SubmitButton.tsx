/* SPDX-License-Identifier: AGPL-3.0-or-later */

import Button from "@mui/material/Button";
import React from "react";

export default ({
  text = "",
}: Readonly<{ text: string }>): React.JSX.Element => (
  <p>
    <Button
      type="submit"
      size="large"
      variant="contained"
      sx={{ textTransform: "capitalize" }}
    >
      {text}
    </Button>
  </p>
);

// Typography - MUI System
// https://mui.com/system/typography/
