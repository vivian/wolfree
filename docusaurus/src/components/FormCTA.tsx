/* SPDX-License-Identifier: AGPL-3.0-or-later */

import InputMathProblems from "./InputMathProblems";
import React from "react";
import SubmitButton from "./SubmitButton";
import submitMathInputForm from "../functions/submitMathInputForm";

export default ({
  autoFocus = false,
  submitText = "",
}: Readonly<{
  autoFocus: boolean;
  submitText: string;
}>): React.JSX.Element => (
  <form onSubmit={submitMathInputForm}>
    <InputMathProblems autoFocus={autoFocus} defaultValue="" />
    <p></p>
    <SubmitButton text={submitText} />
  </form>
);
