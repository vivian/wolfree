/* SPDX-License-Identifier: AGPL-3.0-or-later */

import Button from "@mui/material/Button";
import Link from "@docusaurus/Link";
import React from "react";

export default ({
  to = "",
  text = "",
}: Readonly<{
  to: string;
  text: string;
}>): React.JSX.Element => (
  <p>
    <Link to={to}>
      <Button size="large" sx={{ textTransform: "capitalize" }}>
        <strong>{text}</strong>
        &nbsp; »
      </Button>
    </Link>
  </p>
);

// Typography - MUI System
// https://mui.com/system/typography/

// Docusaurus Client API | Docusaurus
// https://docusaurus.io/docs/docusaurus-core#link
