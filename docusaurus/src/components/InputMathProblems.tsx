/* SPDX-License-Identifier: AGPL-3.0-or-later */

import React from "react";
import TextField from "@mui/material/TextField";

export default ({
  autoFocus = false,
  defaultValue = "",
}: Readonly<{
  autoFocus: boolean;
  defaultValue: string;
}>): React.JSX.Element => (
  <TextField
    name="i"
    type="search"
    autoCapitalize="off"
    autoComplete="off"
    autoCorrect="off"
    spellCheck="false"
    inputProps={{ enterkeyhint: "go" }}
    autoFocus={autoFocus}
    label="&nbsp; Input math problems"
    variant="outlined"
    fullWidth
    defaultValue={defaultValue}
  />
);

// TextField API - Material UI
// https://mui.com/material-ui/api/text-field/

// <input>: The Input (Form Input) element - HTML: HyperText Markup Language | MDN
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input
