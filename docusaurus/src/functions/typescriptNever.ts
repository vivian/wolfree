/* SPDX-License-Identifier: AGPL-3.0-or-later */

export default (typescriptNeverValue: never): never => {
  console.warn({ typescriptNeverValue });

  return typescriptNeverValue;
};

// How do I check that a switch block is exhaustive in TypeScript? - Stack Overflow
// https://stackoverflow.com/questions/39419170/how-do-i-check-that-a-switch-block-is-exhaustive-in-typescript
