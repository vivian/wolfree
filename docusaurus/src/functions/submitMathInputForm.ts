/* SPDX-License-Identifier: AGPL-3.0-or-later */

import React from "react";
import typescriptNever from "./typescriptNever";

export default (event: React.FormEvent<HTMLFormElement>): void => {
  event.preventDefault();
  const htmlFormElement = event.currentTarget;
  const formData = new FormData(htmlFormElement);
  const i = formData.get("i");

  const url = new URL("/input/", window.location.href);

  if (typeof i === "string") {
    url.searchParams.set("i", i);
  } else if (i instanceof File) {
    console.warn({ i });
  } else {
    typescriptNever(i);
  }

  window.location.assign(url);
};
