/* SPDX-License-Identifier: AGPL-3.0-or-later */

// @ts-check

/** @type {import('@docusaurus/plugin-content-docs').SidebarsConfig} */
module.exports = {
  sidebarId: [
    "index",
    {
      type: "html",
      value: '<a class="menu__link" href="/input/">Input math problems</a>',
      className: "menu__list-item",
    },
    "mirror",
    "source",
    "community",
    "acknowledgment",
  ],
};
