/* SPDX-License-Identifier: AGPL-3.0-or-later */

// @ts-check

module.exports = {
  presets: [require.resolve("@docusaurus/core/lib/babel/preset")],
};
