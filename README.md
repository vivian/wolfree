# Self-hosting Wolfree instances with Docker Engine

This guide provides a step-by-step procedure for self-hosting Wolfree. You will use Docker CLI to interact with the Docker daemon through a terminal emulator. Docker Engine automates the execution of the static site generator and running the web server.

## How to self-host Wolfree

To self-host a mirror site of Wolfree instances using Docker Engine, follow these steps:

1. Install a terminal emulator. If you are unfamiliar with terminal emulators, refer to the "[Command line crash course](https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Understanding_client-side_tools/Command_line)" for guidance.

2. Install [Git](https://git-scm.com/) and [Docker Engine](https://docs.docker.com/).

3. Download the source code by executing one of the following commands. Each command will download the source code from a different web server. If one of the commands fails, try the next one until the download succeeds.

   ```shell
   git clone https://gitea.com/wolfree/wolfree-dockerfile.git
   ```

   ```shell
   git clone https://try.gitea.io/wolfree/wolfree-dockerfile.git
   ```

   ```shell
   git clone https://codeberg.org/wolfree/wolfree-dockerfile.git
   ```

   ```shell
   git clone https://git.disroot.org/wolfree/wolfree-dockerfile.git
   ```

   ```shell
   git clone https://next.forgejo.org/wolfree/wolfree-dockerfile.git
   ```

   ```shell
   git clone https://git.kiwifarms.net/wolfree/wolfree-dockerfile.git
   ```

   ```shell
   git clone http://it7otdanqu7ktntxzm427cba6i53w6wlanlh23v5i3siqmos47pzhvyd.onion/wolfree/wolfree-dockerfile.git
   ```

   Reference: [git clone](https://git-scm.com/docs/git-clone)

4. Wait until Git outputs the resolving message similar to the following:

   ```shell
   Receiving objects: 100% (100/100), 1.00 KiB | 1.00 KiB/s, done.
   Resolving deltas: 100% (100/100), done.
   ```

5. Build a Docker image by executing the following command:

   ```shell
   docker build --progress=plain --tag wolfree wolfree-dockerfile
   ```

   This command will generate the website using a static site generator.

   Reference: [docker build](https://docs.docker.com/engine/reference/commandline/build/)

6. Wait until Docker Engine outputs the exporting message similar to the following:

   ```shell
   #35 exporting to image
   #35 exporting layers done
   #35 writing image sha256:0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef done
   #35 naming to docker.io/library/wolfree done
   #35 DONE 0.0s
   ```

7. Run a Docker container by executing the following command:

   ```shell
   docker run --publish 80:80 wolfree
   ```

   This command will host the website using a web server.

   Reference: [docker run](https://docs.docker.com/engine/reference/commandline/run/)

8. Wait until the web server outputs the success message similar to the following:

   ```shell
   > docusaurus@0.0.0 serve
   > docusaurus serve --host localhost --port 80

   [SUCCESS] Serving "build" directory at: http://localhost:80/
   ```

9. Access the mirror site locally on the same computer where the Docker container runs. Open a web browser and navigate to [http://localhost/](http://localhost/). On the home page of the mirror site, enter a math problem in the text box and click the submit button.

Congratulations! You have successfully self-hosted Wolfree.

## How to self-host Libredirect-compatible instances

By default, Wolfree instances are paywall-bypassing and incompatible with Libredirect. To make them Libredirect-compatible and respect paywalls, follow these steps:

1. Install [Git](https://git-scm.com/) and [Docker Engine](https://docs.docker.com/).

2. Download the source code with Git:

   ```shell
   git clone https://gitea.com/wolfree/wolfree-dockerfile.git
   git clone https://try.gitea.io/wolfree/wolfree-dockerfile.git
   git clone https://codeberg.org/wolfree/wolfree-dockerfile.git
   git clone https://git.disroot.org/wolfree/wolfree-dockerfile.git
   git clone https://next.forgejo.org/wolfree/wolfree-dockerfile.git
   git clone https://git.kiwifarms.net/wolfree/wolfree-dockerfile.git
   git clone http://it7otdanqu7ktntxzm427cba6i53w6wlanlh23v5i3siqmos47pzhvyd.onion/wolfree/wolfree-dockerfile.git
   ```

3. Open the following file with a text editor:

   `rust/wolfree_patch_libredirect/src/main.rs`

4. Remove the line containing the `return` keyword. Save the change. The changed code should look like this:

   ```rust
   // Remove the following line to make the instances Libredirect-compatible.
   // Remove the previous line to make the instances Libredirect-compatible.
   ```

5. Build the image and run the container with Docker Engine:

   ```shell
   docker build --progress=plain --tag wolfree wolfree-dockerfile
   docker run --publish 80:80 wolfree
   ```

6. Browse [http://localhost/](http://localhost/). Your instances should have become Libredirect-compatible and respect paywalls. If they remain paywall-bypassing and incompatible with Libredirect, please report the problem in the issue trackers of the repositories.

## Troubleshooting dependency graphs

To regenerate `docusaurus/package.json`, follow these steps:

1. Install [NPM](https://docs.npmjs.com/).

2. Initialize a Docusaurus project with the classic template and the TypeScript variant:

   ```shell
   npm --yes -- init docusaurus docusaurus classic . --typescript
   ```

3. Install MUI, a React component library:

   ```shell
   npm --prefix ./docusaurus/ -- install @mui/material @emotion/react @emotion/styled
   ```

## Glossary of file extensions

- `.md`/`.mdx`: [MarkDown XML](https://mdxjs.com/docs/)
- `.js`/`.jsx`: [JavaScript XML](https://react.dev/learn/writing-markup-with-jsx)
- `.ts`/`.tsx`: [TypeScript XML](https://www.typescriptlang.org/docs/handbook/jsx.html)
- `.rs`: [Rust Programming Language](https://www.rust-lang.org/learn)

## Helpful resources for new developers

If you're new to web development, here are some websites and tools to get you started:

- [MDN Web Docs](https://developer.mozilla.org/en-US/docs/Learn): A comprehensive resource for web developers, offering guides, tutorials, and references for various web technologies.

- [Chrome DevTools - Chrome Developers](https://developer.chrome.com/docs/devtools/): A set of web developer tools built directly into the Google Chrome browser, providing practical features to inspect and debug web applications.

- [TypeScript: JavaScript With Syntax For Types.](https://www.typescriptlang.org/): A strongly typed programming language that builds on JavaScript, giving you better tooling at any scale.

- [Web.Dev](https://web.dev/learn/): A learning platform by Google that offers web development courses, providing practical examples to help you improve your skills.

- [Desktop/PC - Privacy Guides](https://www.privacyguides.org/en/desktop/): A list of recommended Linux distributions with powerful terminal emulators pre-installed by default, empowering you with automating deployment, scaling, and management of containerized applications.

These resources will help you get started. Happy learning!

## UX writing tips

- [Style guide: UX writing best practices](https://m3.material.io/foundations/content-design/style-guide/ux-writing-best-practices): Writing UI text that anyone can understand.

- [UX writing: study guide](https://www.nngroup.com/articles/ux-writing-study-guide/): How to write and present information that aligns with users' needs and online reading behaviors.

- [UX basics: study guide](https://www.nngroup.com/articles/ux-basics-study-guide/): The basics of user experience.

- [Web UX: study guide](https://www.nngroup.com/articles/web-ux-study-guide/): How users interact with the web and how to design web user experiences.

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
